﻿using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Threading.Tasks;

namespace Theatre.Data
{
    public interface ICRS
    {
        Task<DataTable> GetData(MySqlCommand comm);
        Task<DataTable> GetDataAsync(MySqlCommand comm);
        Task<ObservableCollection<string>> GetDataRows(MySqlCommand comm);
        Task<ObservableCollection<string>> GetDataRowsAsync(MySqlCommand comm);
    }

    public class CRSService

    {
        private MySqlConnection conn;
        private readonly IConfiguration _configuration;

        public CRSService(IConfiguration configuration)
        {
            _configuration = configuration;
            conn = new MySqlConnection(_configuration.GetSection("ConnectionStrings").GetSection("SiteContent").Value);
        }

        public Task<DataTable> GetData(MySqlCommand comm)
        {
            DataTable datatable = new DataTable();
            conn.Open();
            if (conn.State == ConnectionState.Open)
            {
                try
                {
                    if (conn != null)
                    {
                        comm.Connection = conn;
                        datatable.Load(comm.ExecuteReader());
                    }
                }
                catch (Exception E)
                {
                    Console.WriteLine("Подключение к базе данных " + E.Message + "\n" + "Статус подключения " + conn.State + "\n" + E.StackTrace);
                }
            }
            conn.Close();
            return Task.FromResult(datatable);
        }

        public async Task<DataTable> GetDataAsync(MySqlCommand comm)
        {
            DataTable datatable = new DataTable();
            await conn.OpenAsync();
            if (conn.State == ConnectionState.Open)
            {
                try
                {
                    if (conn != null)
                    {
                        comm.Connection = conn;
                        datatable.Load(await comm.ExecuteReaderAsync());
                        
                    }
                }
                catch (Exception E)
                {
                    Console.WriteLine("Подключение к базе данных " + E.Message + "\n" + "Статус подключения " + conn.State + "\n" + E.StackTrace);
                }
            }
            await conn.CloseAsync();
            return datatable;
        }

        public Task<ObservableCollection<string>> GetDataRows(MySqlCommand comm)
        {
            var datatable = GetData(comm).Result;
            var rows = new ObservableCollection<string>();

            for (int i = 0; i != datatable.Rows.Count; i++)
            {
                string row = "";
                for (int j = 0; j != datatable.Columns.Count; j++)
                {
                    row += datatable.Rows[i][j].ToString() + "\t";

                }
                rows.Add(row);
            }

            return Task.FromResult(rows);
        }

        public async Task<ObservableCollection<string>> GetDataRowsAsync(MySqlCommand comm)
        {
            var datatable = await GetDataAsync(comm);
            var rows = new ObservableCollection<string>();

            for (int i = 0; i != datatable.Rows.Count; i++)
            {
                string row = "";
                for (int j = 0; j != datatable.Columns.Count; j++)
                {
                    row += datatable.Rows[i][j].ToString() + "\t";

                }
                rows.Add(row);
            }

            return rows;
        }

        public void RunCommand(MySqlCommand comm)
        {
            conn.Open();
            if (conn.State == ConnectionState.Open)
            {
                try
                {
                    comm.Connection = conn;
                    comm.ExecuteNonQuery();
                }
                catch (Exception E)
                {
                    Console.WriteLine("Подключение к базе данных " + E.Message + "\n" + "Статус подключения " + conn.State + "\n" + E.StackTrace);
                }
                conn.Close();
            }
        }

        public async void RunCommandAsync(MySqlCommand comm)
        {
            await conn.OpenAsync();
            if (conn.State == ConnectionState.Open)
            {
                try
                {
                    comm.Connection = conn;
                    await comm.ExecuteNonQueryAsync();
                }
                catch (Exception E)
                {
                    Console.WriteLine("Подключение к базе данных " + E.Message + "\n" + "Статус подключения " + conn.State + "\n" + E.StackTrace);
                }
                await conn.CloseAsync();
            }
        }
    }
}
