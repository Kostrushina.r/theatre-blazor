﻿using System.ComponentModel.DataAnnotations;

namespace Theatre.Data
{
    public class TIcket
    {
        [Required(ErrorMessage = "Выберите спектакль")]
        public string Show { get; set; }
        public int User { get; set; }
    }
}
