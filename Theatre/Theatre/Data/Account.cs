﻿using System;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography;
namespace Theatre.Data
{
    public class Account
    {
        private SHA256 Encryptor= SHA256.Create(); 
        [Required]
        public string login { get; set; }
        [Required]
        [MinLength(5)]
        public string password { get; set; }
        public string сardnum { get; set; }
        public string phone { get; set; }
        public Account() { }
        public Account(string login, string password)
        {
            this.login = login;
            this.password = password;
        }
        public string EnycryptPassword()
        {
            byte[] bytes = Encryptor.ComputeHash(Encoding.UTF8.GetBytes(password));
            StringBuilder StrB = new StringBuilder();
            for (int i = 0; i != bytes.Length; i++)
            {
                StrB.Append(bytes[i].ToString("x2"));
            }
            return StrB.ToString();
        }
    }
}
